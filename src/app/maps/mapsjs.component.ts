import { Component, EventEmitter, Output, Input, SimpleChange } from '@angular/core';
import { AlertController } from 'ionic-angular';

import { Stop } from '../../services/stops/stop';
import { StopsService } from '../../services/stops/stops.service';

@Component({
  selector: 'mapsjs',
  templateUrl: 'mapsjs.html'
})
export class Mapsjs {
  @Output() onChangeBounds: EventEmitter<any> = new EventEmitter();
  @Output() onSelectTerminal: EventEmitter<any> = new  EventEmitter();
  @Output() setStatus: EventEmitter<any> = new  EventEmitter();
  @Input() selected: Stop = null;
  @Input() center: {lat: number, lng: number} = {lat: 51.13141, lng: 1.33697};
  map = null
  stops: Stop[] = [];
  marker = {};

  constructor(private stopsService: StopsService, public alertCtrl: AlertController) {}
  onMapReady(map) {
    this.map = map;
    // HACK: this is because sometimes bounds is undefined
    // setTimeout(() => {
    //   let bounds = this.getBounds()
    //   this.getMarkers(bounds);
    // }, 100)
  }
  getBounds() {
    try {
      let bounds = this.map.getBounds();
      let minlon = bounds.b.b;
      let minlat = bounds.b.f;
      let maxlon = bounds.f.b;
      let maxlat = bounds.f.f;
      return {minlon, minlat, maxlon, maxlat};
    } catch (err) {
      // FIXME: sometimes bounds is undefined
      // return this.getBounds();
    }
  }
  onIdle(event) {
    // HACK: this is because sometimes bounds is undefined
    setTimeout(() => {
      let bounds = this.getBounds()
      this.getMarkers(bounds);
      this.onChangeBounds.emit([bounds]);
    }, 100)
  }
  onBoundsChanged(event) {
    let bounds = this.getBounds();
    console.log('ON BOUNDS', bounds);
    this.getMarkers(bounds);
  }
  onMarkerInit(marker) {
    // console.log('marker', marker);
  }
  onYourLocationInit(marker) {
    console.log('marker', marker);
    marker.nguiMapComponent.openInfoWindow('yourLocation', marker);
  }
  onMapClick(event) {
    console.log(event.latLng);
    event.target.panTo(event.latLng);
  }
  onZoom(event) {
    console.log(event);
  }
  async getMarkers(bounds) {
    try {
      this.setStatus.emit(["Fetching stops..."]);
      let stops = await this.getStops(bounds);
      this.setStatus.emit(["Fetching stops completed."]);
      console.log(stops);
      stops.map(stop => {
        if (this.stops.indexOf(stop) === -1) {
          this.stops.push(stop);
        }
      })
    } catch (err) {
      let alert = this.alertCtrl.create({
        title: 'Error!',
        subTitle: err.toString(),
        buttons: ['OK']
      });
      alert.present();
    }
  }
  async getStops(bounds): Promise<Stop[]> {
    try {
      return await this.stopsService.getStops(bounds);
    } catch (err) {
      throw err
    }
  }

  onMarkerClick(stop, event) {
    this.setSelected(stop, event);
  }

  setSelected(stop, _a?) {
    this.selected = stop;
    this.map.panTo({lat: stop.latitude, lng: stop.longitude});
    this.map.setZoom(20);
    if (_a) {
      var marker = _a.target;
      marker.nguiMapComponent.openInfoWindow('iw', marker);
      this.onSelectTerminal.emit([stop]);
    }
  }
  ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
    if (changes['selected']) {
      if (changes['selected'].currentValue) {
        this.setSelected(changes['selected'].currentValue);
      } else if (changes['selected'].currentValue === null && changes['selected'].previousValue) {
        this.map.setZoom(15);
      }
    }
  }
}
