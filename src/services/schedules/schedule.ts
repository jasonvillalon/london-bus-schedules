export class Schedule {
  mode: string; //"bus",
  line: string; //"135",
  line_name: string; //"135",
  direction: string; //"Old Street Stn",
  operator: string; //"LONDONBUS",
  date: string; //"2017-06-30",
  expected_departure_date: string; //"2017-06-30",
  aimed_departure_time: string; //"06:09",
  expected_departure_time: string; //"06:10",
  best_departure_estimate: string; //"06:10",
  source: string; //"Countdown instant",
  dir: string; //"outbound",
  id: string; //"http://transportapi.com/v3/uk/bus/route/LONDONBUS/135/outbound/490011448Z/2017-06-30/06:09/timetable.json?app_id=915f5f01&app_key=6c790cc8b20f0b394dedf8ba0ff8353c",
  operator_name: string; //"London Buses"
}
