import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Lines } from './lines';

@Injectable()
export class SchedulesService {
  private getUrl(atcocode) {
    return `http://transportapi.com/v3/uk/bus/stop/${atcocode}/live.json?group=route&api_key=e62a3e7c66eb63512926e737369567e2&app_id=01c4787b`;  // URL to web api
  }

  constructor(private http: Http) { }

  async getSchedule(atcocode): Promise<Lines[]> {
    try {
      let response = await this.http.get(this.getUrl(atcocode)).toPromise();
      let departures = response.json().departures;
      let lines: Lines[] = [];
      Object.keys(departures).map(line => {
        lines.push({line, schedules: departures[line]});
      });
      return lines;
    } catch (err) {
      throw err.message || err;
    }
  } // get schedules
}
