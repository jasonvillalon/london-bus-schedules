import { Component, OnInit } from '@angular/core';
import { NavController, ActionSheetController, ModalController } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';

import { ListPage } from '../list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {
  stop = null;
  bounds = null;
  currentLocation = null;
  status: string = "";
  constructor(private geolocation: Geolocation, public navCtrl: NavController, public actionSheetCtrl: ActionSheetController, public modalCtrl: ModalController) {

  }
  setSelectedStop(stop) {
    this.stop = stop;
  }
  resetSearch() {
    this.stop = null;
  }
  onChangeBounds(bounds) {
    this.bounds = bounds;
  }
  ngOnInit() {
    this.getCurrentLocation()
  }
  async getCurrentLocation() {
    this.status = "Finding your current location...";
    let response = await this.geolocation.getCurrentPosition();
    this.status = "Location Found.";
    console.log(response);
    this.currentLocation = {lat: 51.13141, lng: 1.33697} // {lat: response.coords.latitude, lng: response.coords.longitude}
  }
  setStatus(status: string) {
    this.status = status;
  }
  async onTerminalSelected(stop) {
    let actionSheet = this.actionSheetCtrl.create({
     title: 'Terminal',
     buttons: [
       {
         text: 'Show Schedules',
         handler: () => {
           let listModal = this.modalCtrl.create(ListPage, {stop});
           listModal.present();
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
  }
}
