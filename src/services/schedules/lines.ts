import { Schedule } from './schedule';

export class Lines {
  line: string;
  schedules: Schedule[];
}
