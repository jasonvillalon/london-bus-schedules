import { Component, OnInit } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

import { Stop } from '../../services/stops/stop';
import { Lines } from '../../services/schedules/lines';
import { Schedule } from '../../services/schedules/schedule';
import { SchedulesService } from '../../services/schedules/schedule.service';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage implements OnInit {
  selectedStop: Stop = null;
  lines: Lines[] = [];
  fetching: boolean = false;
  constructor(public viewCtrl: ViewController, public navParams: NavParams, private scheduleService: SchedulesService) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedStop = navParams.get('stop');
  }
  ngOnInit(): void {
    this.getSchedules();
  }
  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    // this.navCtrl.push(ListPage, {
    //   item: item
    // });
  }
  close() {
    this.viewCtrl.dismiss()
  }
  async getSchedules() {
    this.fetching = true;
    this.lines = await this.scheduleService.getSchedule(this.selectedStop.atcocode);
    this.fetching = false;
  }
}
