import { Component, Input, EventEmitter, Output } from '@angular/core';

import { Stop } from '../../services/stops/stop';
import { StopsService } from '../../services/stops/stops.service';

@Component({
  selector: 'search-stops',
  templateUrl: 'search-stops.html'
})
export class SearchStops {
  stops: Stop[] = [];
  @Input() placeholder:string;
  // TODO: make a type for bounds
  @Input() bounds:Object;
  @Output() onSelect: EventEmitter<any> = new EventEmitter();
  @Output() setStatus: EventEmitter<any> = new  EventEmitter();
  constructor(private stopsService: StopsService) {

  }

  async getStops(): Promise<void> {
    this.setStatus.emit(["Fetching stops..."]);
    this.stops = await this.stopsService.getStops(this.bounds);
    this.setStatus.emit(["Fetching stops completed."]);
    console.log('GET STOPS', this.bounds, this.stops);
  }

  getItems(ev: any) {
    // set val to the value of the searchbar
    let val = ev.target.value;
    if (val.length > 0) {
      this.getStops().then(() => {
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.stops = this.stops.filter((stop) => {
            return (stop.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      })
    } else {
      this.stops = [];
    }
  }

  itemSelected(stop: Stop) {
    this.onSelect.emit([stop]);
  }
}
