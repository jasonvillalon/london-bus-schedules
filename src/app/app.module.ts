import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { Maps } from './maps/maps.component';
import { Mapsjs } from './maps/mapsjs.component';
import { SearchStops } from './search-stops/search-stops.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleMaps } from '@ionic-native/google-maps';
import { HttpModule } from '@angular/http';

import { NguiMapModule} from '@ngui/map';

import { StopsService } from '../services/stops/stops.service';
import { SchedulesService } from '../services/schedules/schedule.service';

@NgModule({
  declarations: [
    Maps,
    Mapsjs,
    MyApp,
    SearchStops,
    HomePage,
    ListPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyCl5MQlL8yuSBQfBRdCx02NtJ3A8H9YCSo'})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Maps,
    Mapsjs,
    MyApp,
    SearchStops,
    HomePage,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GoogleMaps,
    StopsService,
    SchedulesService,
    Geolocation
  ]
})
export class AppModule {}
