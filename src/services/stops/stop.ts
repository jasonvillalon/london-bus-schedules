export class Stop {
  atcocode: string;
  mode: string;
  name: string;
  stop_name: string;
  smscode: string;
  bearing: string;
  locality: string;
  indicator: string;
  longitude: number;
  latitude: number;
  distance: number;
}
