import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Stop } from './stop';

@Injectable()
export class StopsService {
  // private key = '6c790cc8b20f0b394dedf8ba0ff8353c';
  // private app_id = '915f5f01';
  // private headers = new Headers({'Content-Type': 'application/json'});
  private url = 'http://transportapi.com/v3/uk/bus/stops/bbox.json?api_key=e62a3e7c66eb63512926e737369567e2&app_id=01c4787b';  // URL to web api

  constructor(private http: Http) { }

  async getStops(bounds): Promise<Stop[]> {
    try {
      let query = `&minlon=${bounds.minlon}&minlat=${bounds.minlat}&maxlon=${bounds.maxlon}&maxlat=${bounds.maxlat}`;
      console.log('REQUESTING', query);
      let response = await this.http.get(`${this.url}${query}`).toPromise();
      console.log('response', response.json().stops);
      return response.json().stops;
    } catch (err) {
      throw err.message || err;
    }
  } // get stops
}
