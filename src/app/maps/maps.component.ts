import { Component } from '@angular/core';

import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

@Component({
  selector: 'maps',
  templateUrl: 'maps.html'
})
export class Maps {
  map: GoogleMap = null;

  defaultLat = 48.856614;
  defaultLng = 2.3522219;

  constructor(private googleMaps: GoogleMaps,) {

  }

  // Load map only after view is initialized
  ngAfterViewInit() {
   this.loadMap();
  }

  loadMap() {
    let element: HTMLElement = document.getElementById('map');

    this.map = this.googleMaps.create(element);
    console.log(this.map);
    this.map.one(GoogleMapsEvent.MAP_READY).then(
      (map) => {
        console.log('Map is ready!', map);
        // this.getStops();
      }
    );

    // create LatLng object
    let defaultLatLng: LatLng = new LatLng(this.defaultLat,this.defaultLng);

    // create CameraPosition
    let position: CameraPosition = {
      target: defaultLatLng,
      zoom: 18,
      tilt: 30
    };

    // move the map's camera to position
    this.map.moveCamera(position);

    // create new marker
    let markerOptions: MarkerOptions = {
      position: defaultLatLng,
      title: 'London'
    };

    this.map.addMarker(markerOptions)
    .then((marker: Marker) => {
      marker.showInfoWindow();
    });
  }
}
